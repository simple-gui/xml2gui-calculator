
"""
---------- Dedicated to Mary our mother in spirit, and to her son Jesus our brother in spirit.
---------- Just like Adam and Eve. A new beginning...
"""

import sys
sys.path.append('./libs')

import xml2gui as g

# You can use an online XML validator to validate your XML.
xml = open('app.xml', mode='r', encoding='utf-8').read()


# def listener_click(id, msg):
    # print(f"CLICK   id: {id}, msg: {msg}")
    
    
# def listener_dbclick(id, msg):
    # print(f"DBCLICK id: {id}, msg: {msg}")
    
    
# def listener_hover(id, msg):
    # print(f"HOVER   id: {id}, msg: {msg}")
    
    
def listener_drag(id, msg):
    print(f"DRAG    id: {id}, msg: {msg}")
    if id == "btAC":
        list = msg.split("|")
        g.mcxml_set_left(id, list[1])
        g.mcxml_set_top(id, list[2])
        g.mcxml_set()
    
    
# def listener_keyboard(id, msg):
    # print(f"KEYBOARD    id: {id}, msg: {msg}")
    
    
def listener_resize(id, msg):
    print(f"RESIZE  id: {id}, msg: {msg}")


# g.mcxml_listener_click(listener_click)
# g.mcxml_listener_dbclick(listener_dbclick)
# g.mcxml_listener_hover(listener_hover)
g.mcxml_listener_drag(listener_drag)
# g.mcxml_listener_keyboard(listener_keyboard)
g.mcxml_listener_resize(listener_resize)


if __name__ == '__main__':
    g.mcxml_loop(xml);
    